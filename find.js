function find(elements, cb) {
    // Do NOT use .includes, to complete this function.
    // Look through each value in `elements` and pass each element to `cb`.
    // If `cb` returns `true` then return that element.
    // Return `undefined` if no elements pass the truth test.

    let errorMessage;

    if(!Array.isArray(elements)) {
        errorMessage = "First argument must be an Array";
        return errorMessage;
    }

    if(typeof cb !== "function") {
        errorMessage = "Second argument must be a function";
        return errorMessage;
    }

    for(let index=0; index<elements.length; index++) {
        let foundElement;
        if(cb(elements[index])) {
            foundElement = elements[index];
            return foundElement;
        }
    } 
}

module.exports = find;