function filter(elements, cb) {
    // Do NOT use .filter, to complete this function.
    // Similar to `find` but you will return an array of all elements that passed the truth test
    // Return an empty array if no elements pass the truth test

    let errorMessage;

    if(!Array.isArray(elements)) {
        errorMessage = "First argument must be an Array";
        return errorMessage;
    }

    if(typeof cb !== "function") {
        errorMessage = "Second argument must be a function";
        return errorMessage;
    }
    
    let filteredElements = [];

    for(let index=0; index<elements.length; index++) {
        if(cb(elements[index])) {
            filteredElements.push(elements[index]);
        }
    }

    return filteredElements;
}

module.exports = filter;