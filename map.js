function map(elements, cb) {
    // Do NOT use .map, to complete this function.
    // How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
    // Produces a new array of values by mapping each value in list through a transformation function (iteratee).
    // Return the new array.

    let errorMessage;

    if(!Array.isArray(elements)) {
        errorMessage = "First argument must be an Array";
        return errorMessage;
    }

    if(typeof cb !== "function") {
        errorMessage = "Second argument must be a function";
        return errorMessage;
    }

    let outputArray = new Array(elements.length);
    for(let index=0; index<elements.length; index++) {
        let result = cb(elements[index]);
        outputArray[index] = result;
    }
    return outputArray;
}

module.exports = map;