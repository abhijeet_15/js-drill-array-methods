function flatten(elements) {
    // Flattens a nested array (the nesting can be to any depth).
    // Hint: You can solve this using recursion.
    // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];

    let errorMessage;

    if(!Array.isArray(elements)) {
        errorMessage = "First argument must be an Array";
        return errorMessage;
    }

    if(typeof cb !== "function") {
        errorMessage = "Second argument must be a function";
        return errorMessage;
    }

    let result = [];
    for(let index=0; index<elements.length; index++) {
        if(Array.isArray(elements[index])) {
            result.push(...flatten(elements[index]));
        }
        else {
            result.push(elements[index]);
        }
    }

    return result;
}

module.exports = flatten;