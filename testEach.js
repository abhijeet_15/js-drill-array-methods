const items = require("./items");
const each = require("./each");

const result = each(items, (item, index) => console.log(`${item} is at index ${index}`));
if(result === "First argument must be an Array" || result === "Second argument must be a function") {
    console.log(result);
}

