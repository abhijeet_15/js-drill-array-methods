const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'
const flatten = require("./flatten");

let flattenedArray = flatten(nestedArray, (item) => item);
console.log(flattenedArray);